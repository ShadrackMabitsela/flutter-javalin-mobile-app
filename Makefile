SHELL=/bin/bash

# Makefile variable(s).
test-server = test
package-server = package
deploy-server = deploy

# Makefile execution arguments.
test-web-api-server: $(test-server)
package-web-api-server: $(package-server)
deploy-web-api-server: $(deploy-server)

# Makefile targets.
# Maven clean the project.
test:
	cd ./QuteQuotesServer && mvn clean && mvn test

# Package the Javalin web-api into a .jar file [QuteQuotesServer-0.1.0-RELEASE.jar].
package:
	cd ./QuteQuotesServer && mvn clean && mvn package

# Push docker image to Container Registry.
deploy:
	docker build -t gitlab.wethinkco.de:5050/smabitse/qute-quotes-flutter-javalin-project:latest .
	docker login gitlab.wethinkco.de:5050
	docker push gitlab.wethinkco.de:5050/smabitse/qute-quotes-flutter-javalin-project:latest
	docker logout
