FROM ubuntu:latest

LABEL maintainer="Shadrack Mabitsela [smabitse]"

RUN apt-get update
RUN apt-get install -y openjdk-11-jdk
RUN apt-get install -y maven
RUN apt-get install -y make
RUN apt-get install -y curl

ADD QuteQuotesServer/target/QuteQuotesServer-0.1.0-RELEASE.jar /srv/QuteQuotesServer-0.1.0-RELEASE.jar
ADD QuteQuotesServer/QuteQuotes.db /srv/QuteQuotes.db
WORKDIR /srv

EXPOSE 5050
CMD ["java", "-jar", "QuteQuotesServer-0.1.0-RELEASE.jar"]
