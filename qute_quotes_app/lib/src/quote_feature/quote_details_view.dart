import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

/// Displays detailed information about a SampleItem.
class QuoteDetailsView extends StatefulWidget {
  const QuoteDetailsView({Key? key}) : super(key: key);

  static const routeName = '/quote_details';

  @override
  State<QuoteDetailsView> createState() => _QuoteDetailsViewState();
}

class _QuoteDetailsViewState extends State<QuoteDetailsView> {
  Map quoteDetailsMap = {};

  @override
  Widget build(BuildContext context) {
    // This is where we receive the actual arguments from
    // 'restorablePushNamed' in the quote_card.dart

    quoteDetailsMap = ModalRoute.of(context)!.settings.arguments as Map;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: const Text(
          "Qute Quote",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xfffc354c),
              Color(0xff0abfbc),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 20.0),
              child: Card(
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                elevation: 15.0,
                shadowColor: Colors.black,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Ink.image(
                            image: const AssetImage('assets/images/retro.jpg'),
                            child: InkWell(
                              onTap: () {},
                            ),
                            height: 240,
                            fit: BoxFit.cover),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: Text(
                                  quoteDetailsMap["author_name"],
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 30,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        quoteDetailsMap["qute_quote"],
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    ButtonBar(
                      alignment: MainAxisAlignment.end,
                      children: [
                        MaterialButton(
                          elevation: 0,
                          textColor: Colors.grey,
                          splashColor: Colors.red,
                          child: Row(
                            children: const [
                              Icon(Icons.close),
                              Text(
                                "Close",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
