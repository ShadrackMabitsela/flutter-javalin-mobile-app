import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:getwidget/components/avatar/gf_avatar.dart';
import 'package:getwidget/shape/gf_avatar_shape.dart';
import 'package:provider/provider.dart';
import 'package:qute_quotes_app/src/models/quote_model.dart';
import 'package:qute_quotes_app/src/quote_feature/quote_details_view.dart';
import 'package:sliding_sheet/sliding_sheet.dart';

import '../settings/settings_view.dart';

// The widgets itself.
/// Displays a list of quotes.
class AllQuotesListView extends StatefulWidget {
  const AllQuotesListView({
    Key? key,
  }) : super(key: key);

  static const routeName = '/quotes';

  @override
  State<AllQuotesListView> createState() => _AllQuotesListViewState();
}

// The object associated with the widget.
class _AllQuotesListViewState extends State<AllQuotesListView> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  // Initialise a scroll controller [needed for the 'Scrollbar' widget].
  final ScrollController _scrollController = ScrollController();

  // Used to retrieve the data from the bottom-sheet to use somewhere else.
  TextEditingController quoteAuthorNameController = TextEditingController();
  TextEditingController newQuoteController = TextEditingController();

  String quoteAuthorName = "";
  String newQuote = "";

  // Snackbar for adding a new quote.
  final successSnackbar = SnackBar(
    content: const Text("New quote successfully added.", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
    backgroundColor: Colors.green,
    duration: const Duration(seconds: 6),
    action: SnackBarAction(
      label: 'Close',
      onPressed: () {},
    ),
  );

  final errorSnackbar = SnackBar(
    content: const Text("Unable to successfully add a new quote.", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
    backgroundColor: Colors.red,
    duration: const Duration(seconds: 6),
    action: SnackBarAction(
      label: 'Close',
      onPressed: () {},
    ),
  );

  final deleteSuccessSnackbar = SnackBar(
    content: const Text("Quote successfully deleted.", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
    backgroundColor: Colors.green,
    duration: const Duration(seconds: 6),
    action: SnackBarAction(
      label: 'Close',
      onPressed: () {},
    ),
  );

  final deleteErrorSnackbar = SnackBar(
    content: const Text("Unable to successfully delete the quote.", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
    backgroundColor: Colors.red,
    duration: const Duration(seconds: 6),
    action: SnackBarAction(
      label: 'Close',
      onPressed: () {},
    ),
  );

  // NB We must dispose of the controllers once the bottom-sheet and the
  // widgets inside it have been disposed.
  @override
  void dispose() {
    // Dispose what you need to dispose FIRST then call 'super.dispose();'.
    // This is necessary to free up the resources.
    quoteAuthorNameController.clear();
    newQuoteController.clear();
    quoteAuthorNameController.dispose();
    newQuoteController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Get the list of quotes [remember 'fetchAllQuotes' is async].
    context.read<QuoteData>().fetchAllQuotes;

    return Scaffold(
      // We need the scaffold key to get the context of the scaffold
      // in order to start showing the bottom-sheet.
      key: scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: const Text(
          'All Qute Quotes',
          style: TextStyle(
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
              context.read<QuoteData>().initialValues();
              context.read<QuoteData>().fetchAllQuotes;
            },
          ),
          const SizedBox(width: 10.0),
          IconButton(
            icon: const Icon(Icons.fiber_new_outlined),
            iconSize: 35,
            onPressed: () => showSlidingBottomSheet(
              context,
              builder: (BuildContext context) => SlidingSheetDialog(
                cornerRadius: 20.0,
                avoidStatusBar: true,
                snapSpec: const SnapSpec(
                  initialSnap: 1,
                  snappings: [0.5, 0.75, 1],
                ),
                builder: (context, state) => Material(
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xfffc354c),
                          Color(0xff0abfbc),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ListView(
                        shrinkWrap: true,
                        primary: false,
                        padding: const EdgeInsets.all(10.0),
                        children: [
                          const SizedBox(height: 15.0),
                          TextField(
                            controller: quoteAuthorNameController,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.text,
                            cursorColor: Colors.white,
                            autocorrect: true,
                            style: const TextStyle(color: Colors.white),
                            decoration: const InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 2.0),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20.0),
                                ),
                              ),
                              labelText: "Author Name: [Or Enter \"Unknown\"]",
                              labelStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          const SizedBox(height: 30.0),
                          Scrollbar(
                            controller: _scrollController,
                            isAlwaysShown: true,
                            child: TextField(
                              controller: newQuoteController,
                              scrollController: _scrollController,
                              keyboardType: TextInputType.multiline,
                              cursorColor: Colors.white,
                              maxLines: null,
                              autocorrect: true,
                              style: const TextStyle(color: Colors.white),
                              onChanged: (s) => {},
                              decoration: const InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20.0),
                                  ),
                                ),
                                labelText: "Enter New Quote:",
                                labelStyle: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 20.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              MaterialButton(
                                elevation: 0,
                                textColor: Colors.white,
                                splashColor: Colors.green,
                                child: Row(
                                  children: const [
                                    Icon(Icons.add),
                                    Text(
                                      "Add",
                                      style: TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                onPressed: () async {
                                  setState(() {
                                    quoteAuthorName = quoteAuthorNameController.text;
                                    newQuote = newQuoteController.text;
                                  });

                                  context.read<QuoteData>().setAuthor(quoteAuthorName);
                                  context.read<QuoteData>().setQuote(newQuote);
                                  int? code = await context.read<QuoteData>().addNewQuote;

                                  if (code == 201) {
                                    context.read<QuoteData>().initialValues();
                                    context.read<QuoteData>().fetchAllQuotes;

                                    Navigator.pop(context);
                                    ScaffoldMessenger.of(context).showSnackBar(successSnackbar);
                                  } else {
                                    Navigator.pop(context);
                                    ScaffoldMessenger.of(context).showSnackBar(errorSnackbar);
                                  }
                                },
                              ),
                              const SizedBox(width: 20.0),
                              MaterialButton(
                                elevation: 0,
                                textColor: Colors.white,
                                splashColor: Colors.red,
                                child: Row(
                                  children: const [
                                    Icon(Icons.cancel),
                                    Text("Cancel", style: TextStyle(fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                onPressed: () {
                                  quoteAuthorNameController.clear();
                                  newQuoteController.clear();
                                  // Navigator.of(context).pop();
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                headerBuilder: (BuildContext context, SheetState state) => Material(
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const SizedBox(height: 10.0),
                        Center(
                          child: Container(
                            width: 32.0,
                            height: 8.0,
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.black),
                          ),
                        ),
                        const SizedBox(height: 10.0),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(width: 10.0),
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              // Navigate to the settings page. If the user leaves and returns
              // to the app after it has been killed while running in the
              // background, the navigation stack is restored.
              Navigator.pushNamed(context, SettingsView.routeName);
            },
          ),
        ],
      ),

      // Enable the user to be able to drag and refresh the quote list.
      body: RefreshIndicator(
        onRefresh: () async {
          await context.read<QuoteData>().fetchAllQuotes;
        },
        // To work with lists that may contain a large number of items, it’s best
        // to use the ListView.builder constructor.
        //
        // In contrast to the default ListView constructor, which requires
        // building all Widgets up front, the ListView.builder constructor lazily
        // builds Widgets as they’re scrolled into view.
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color(0xfffc354c),
                Color(0xff0abfbc),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: Consumer<QuoteData>(
            builder: (context, value, child) {
              // if 'allQuotes' is empty and there is no errors.
              return value.allQuotes.isEmpty && !value.error
                  ? const Center(child: CircularProgressIndicator())
                  // if there was an error.
                  : value.error
                      ? const Text("Error: Could not load quotes.")
                      // All's good.
                      : ListView.builder(
                          // Providing a restorationId allows the ListView to restore the
                          // scroll position when a user leaves and returns to the app after it
                          // has been killed while running in the background.
                          restorationId: 'allQuotesListView',
                          itemCount: value.allQuotes.length,
                          itemBuilder: (BuildContext context, int index) {
                            // final item = value.allQuotes[index];
                            // return QuoteCard(quoteMap: value.allQuotes[index]);
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: InkWell(
                                child: Card(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                                  shadowColor: Colors.white,
                                  elevation: 5.0,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            const GFAvatar(
                                              // Display the Flutter Logo image asset.
                                              backgroundImage: AssetImage('assets/images/quote_symbol.png'),
                                              shape: GFAvatarShape.standard,
                                            ),
                                            const SizedBox(width: 15.0),
                                            Flexible(
                                                child: Text(
                                              "${value.allQuotes[index]["qute_quote"]}",
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            )),
                                          ],
                                        ),
                                        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                          Flexible(
                                            child: Text(
                                              "${value.allQuotes[index]["author_name"]}",
                                              style: const TextStyle(
                                                fontStyle: FontStyle.italic,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0,
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                            icon: const Icon(Icons.delete),
                                            onPressed: () {
                                              showDialog(
                                                barrierDismissible: false,
                                                context: context,
                                                builder: (context) {
                                                  return AlertDialog(
                                                    contentPadding: const EdgeInsets.all(20.0),
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(20.0),
                                                      side: const BorderSide(color: Colors.grey, width: 5),
                                                    ),
                                                    title: const Text("Are You sure you want to delete?"),
                                                    content: Text(
                                                        "Quote: \"${value.allQuotes[index]["qute_quote"]}\"\n\nBy: ${value.allQuotes[index]["author_name"]}"),
                                                    actions: [
                                                      Padding(
                                                        padding: const EdgeInsets.all(10.0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: [
                                                            ElevatedButton.icon(
                                                              icon: const Icon(Icons.thumb_up),
                                                              label: const Text("OK"),
                                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.grey)),
                                                              onPressed: () async {
                                                                int quoteId = value.allQuotes[index]["quote_id"];
                                                                context.read<QuoteData>().setQuoteID(quoteId);
                                                                int? code = await context.read<QuoteData>().deleteQuote;

                                                                if (code == 200) {
                                                                  context.read<QuoteData>().initialValues();
                                                                  context.read<QuoteData>().fetchAllQuotes;
                                                                  Navigator.pop(context);
                                                                  ScaffoldMessenger.of(context).showSnackBar(deleteSuccessSnackbar);
                                                                } else {
                                                                  Navigator.pop(context);
                                                                  ScaffoldMessenger.of(context).showSnackBar(deleteErrorSnackbar);
                                                                }
                                                              },
                                                            ),
                                                            ElevatedButton.icon(
                                                              icon: const Icon(Icons.thumb_down),
                                                              label: const Text("NO"),
                                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.grey)),
                                                              onPressed: () {
                                                                Navigator.pop(context);
                                                              },
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                        ]),
                                      ],
                                    ),
                                  ),
                                ),
                                onDoubleTap: () {
                                  Navigator.restorablePushNamed(
                                    context,
                                    QuoteDetailsView.routeName,
                                    arguments: {
                                      "author_name": value.allQuotes[index]["author_name"],
                                      "qute_quote": value.allQuotes[index]["qute_quote"],
                                    },
                                  );
                                },
                              ),
                            );
                          },
                        );
            },
          ),
        ),
      ),
    );
  }
}
