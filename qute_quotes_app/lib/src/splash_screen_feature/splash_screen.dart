import 'package:flutter/material.dart';
import 'package:qute_quotes_app/src/quote_feature/all_quotes_view.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

class SplashScreenPageView extends StatelessWidget {
  const SplashScreenPageView({Key? key}) : super(key: key);

  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    return SplashScreenView(
      navigateRoute: const AllQuotesListView(),
      duration: 5000,
      imageSize: 150,
      imageSrc: "assets/images/quote_symbol3.png",
      text: "Qute Quotes",
      textType: TextType.ColorizeAnimationText,
      textStyle: const TextStyle(
        fontSize: 50.0,
      ),
      colors: const [
        Colors.purple,
        Colors.blue,
        Colors.yellow,
        Colors.red,
      ],
      backgroundColor: Colors.white,
    );
  }
}
