import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:getwidget/components/avatar/gf_avatar.dart';
import 'package:getwidget/shape/gf_avatar_shape.dart';
import 'package:qute_quotes_app/src/quote_feature/quote_details_view.dart';

class QuoteCard extends StatelessWidget {
  const QuoteCard({Key? key, required this.quoteMap}) : super(key: key);

  final Map<String, dynamic> quoteMap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          shadowColor: Colors.white,
          elevation: 5.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const GFAvatar(
                      // Display the Flutter Logo image asset.
                      backgroundImage: AssetImage('assets/images/quote_symbol.png'),
                      shape: GFAvatarShape.standard,
                    ),
                    const SizedBox(width: 15.0),
                    Flexible(
                        child: Text(
                      "${quoteMap["qute_quote"]}",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                  ],
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Flexible(
                    child: Text(
                      "${quoteMap["author_name"]}",
                      style: const TextStyle(
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () {},
                  ),
                ]),
              ],
            ),
          ),
        ),
        onDoubleTap: () {
          Navigator.restorablePushNamed(
            context,
            QuoteDetailsView.routeName,
            arguments: {
              "author_name": quoteMap["author_name"],
              "qute_quote": quoteMap["qute_quote"],
            },
          );
        },
      ),
    );
  }
}
