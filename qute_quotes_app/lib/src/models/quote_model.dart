import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

// curl --request GET 'http://localhost:5000/quotes' --include [server response].
// [
// {"author_name":"Stephen King","quote_id":1,"qute_quote":"Get busy living or get busy dying."},
// {"author_name":"Henry Ford","quote_id":2,"qute_quote":"Whether you think you can or you think you can not, you are right."},
// {"author_name":"Wayne Gretzky","quote_id":3,"qute_quote":"You miss 100 percent of the shots you never take."},
// {"author_name":"Lao Tzu","quote_id":4,"qute_quote":"The journey of a thousand miles begins with one step."},
// {"author_name":"Mahatma Gandhi","quote_id":5,"qute_quote":"You must be the change you wish to see in the world."},
// {"author_name":"Mae West","quote_id":6,"qute_quote":"You only live once, but if you do it right, once is enough."},
// {"author_name":"Unknown","quote_id":7,"qute_quote":"There is no snooze button on a cat who wants breakfast."},
// {"author_name":"Robert A. Heinlein","quote_id":8,"qute_quote":"Never try to out-stubborn a cat."},
// {"author_name":"Robert Byrne","quote_id":9,"qute_quote":"To err is human, to purr is feline."},
// {"author_name":"The Dog","quote_id":10,"qute_quote":"Woof"}
// ]
class QuoteData with ChangeNotifier {
  List<dynamic> _allQuotes = <dynamic>[];
  bool _error = false;
  String _errorMessage = "";
  String _author = "";
  String _quote = "";
  int _quoteId = 0;

  List<dynamic> get allQuotes => _allQuotes;
  bool get error => _error;
  String get errorMessage => _errorMessage;

  String setAuthor(String author) => _author = author;
  String setQuote(String quote) => _quote = quote;
  int setQuoteID(int quoteId) => _quoteId = quoteId;

  Future<void> get fetchAllQuotes async {
    final response = await http.get(Uri.parse('http://localhost:5000/quotes'));

    if (response.statusCode == 200) {
      try {
        _allQuotes = jsonDecode(response.body);
        _error = false;
      } catch (err) {
        _error = true;
        _errorMessage = err.toString();
        _allQuotes = [];
      }
    } else {
      _error = true;
      _errorMessage = "'fetchAllQuotes[get]' request failed with status: ${response.statusCode}";
      _allQuotes = [];
    }
    notifyListeners();
  }

  void initialValues() {
    _allQuotes = [];
    _error = false;
    _errorMessage = "";
    notifyListeners();
  }

  Future<int?> get addNewQuote async {
    _author = _author.trim();
    _quote = _quote.trim();

    String uri = "http://localhost:5000/quote/$_author/$_quote";
    uri = Uri.encodeFull(uri);

    var response = await http.post(Uri.parse(uri));
    notifyListeners();
    return response.statusCode;
  }

  Future<int?> get deleteQuote async {
    String uri = "http://localhost:5000/quote/$_quoteId";

    var response = await http.delete(Uri.parse(uri));
    notifyListeners();
    return response.statusCode;
  }
}
