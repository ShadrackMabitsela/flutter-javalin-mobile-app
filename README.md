# Qute quotes flutter javalin project

Year 2, brownfields development, iteration 5, individual exercise: QuteQuotes. This project contains a client-side flutter app that gets quotes to 
display & perform CRUD operations on the quotes from a javalin web-api server.

# NB, Docker Setup.
***
### In order to persist data when running the qute quote web-api docker image.

Create a volume by using the docker volume create command.
- `docker volume create qute-quote-db`

If you were already running the docker image stop it using `docker stop <id>` (e.g. `docker stop b5366f9b8b72`), as it is still running without 
   using the persistent volume.

Start the <mark>registry.gitlab.com/shadrackmabitsela/flutter-javalin-mobile-app</mark> container, but add the -v flag to specify a 
volume mount. 
you will use the named volume and mount it to /etc/qute-quote, which will capture all files created at the path. Example below:
- `docker run -p 5000:5050 -v qute-quote-db:/srv/ b5366f9b8b72`

Once the container starts up, you can move to the flutter setup section.

# Flutter App Setup
***
### In order for the flutter qute quotes app to be able to connect to the Javalin web-api server.
### NB, For android mobile device ONLY!

-> Install ADB on LINUX.
- **Debian / Ubuntu:** `sudo apt install android-tools-adb`
- **Centos / Fedora / openSUSE:** `sudo dnf install android-tools`
- **ArchLinux and derivatives:** `sudo pacman -Sy android-tools`

-> Once ADB is installed & BEFORE you run the flutter qute quotes app run the following command in the terminal.
- `adb reverse tcp:5000 tcp:5000`

-> Now you can run the flutter qute quotes app & it should be able to connect to the Javalin web-api server.
