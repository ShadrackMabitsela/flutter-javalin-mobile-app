package za.co.wethinkcode.qute.quotes.tests;

import org.assertj.core.api.SoftAssertions;
import za.co.wethinkcode.qute.quotes.server.DomainLayer.Quote;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class QuoteClassTests {
    @Test
    @DisplayName("Should create an instance of 'Quote' class.")
    @Order(1)
    void createQuoteObject() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getClass()).isEqualTo(Quote.class);
        soft.assertThat(quote.toString()).contains("za.co.wethinkcode.qute.quotes.server.DomainLayer.Quote");
        soft.assertAll();
    }

    @Test
    @DisplayName("Should check author name.")
    @Order(2)
    void checkAuthorName() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getAuthorName()).isEqualTo("Unknown");
        soft.assertAll();
    }

    @Test
    @DisplayName("Should check quote.")
    @Order(3)
    void checkQuote() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getQuote()).isEqualTo("Unknown author quote.");
        soft.assertAll();
    }

    @Test
    @DisplayName("Should set author name.")
    @Order(4)
    void setAuthorName() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        quote.setAuthorName("Author1");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getAuthorName()).isEqualTo("Author1");
        soft.assertAll();
    }

    @Test
    @DisplayName("Should set quote.")
    @Order(5)
    void setQuote() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        quote.setQuote("Author1 quote.");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getQuote()).isEqualTo("Author1 quote.");
        soft.assertAll();
    }

    @Test
    @DisplayName("Should check Quote data.")
    @Order(5)
    void checkQuoteData() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getAuthorName()).isEqualTo("Unknown");
        soft.assertThat(quote.getQuote()).isEqualTo("Unknown author quote.");
        soft.assertAll();
    }

    @Test
    @DisplayName("Should check Quote data.")
    @Order(5)
    void setQuoteData() {
        Quote quote = Quote.create("Unknown", "Unknown author quote.");

        quote.setAuthorName("Author2");
        quote.setQuote("Author2 quote.");

        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(quote.getAuthorName()).isEqualTo("Author2");
        soft.assertThat(quote.getQuote()).isEqualTo("Author2 quote.");
        soft.assertAll();
    }
}
