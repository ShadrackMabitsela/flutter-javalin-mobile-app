package za.co.wethinkcode.qute.quotes.server.DomainLayer;

public class Quote {
    private String author;
    private String quote;

    public String getQuote() { return this.quote; }
    public String getAuthorName() { return this.author; }

    public void setQuote(String text) { this.quote = text; }
    public void setAuthorName(String author) { this.author = author; }

    /**
     * Use this convenient factory method to create a new quote.
     * Warning the ID will be null!
     * @param author the name of the person that said the text
     * @param quote the text of the quote
     * @return a Quote object
     */
    public static Quote create(String author, String quote) {
        Quote newQuote = new Quote();
        newQuote.setQuote(quote);
        newQuote.setAuthorName(author);
        return newQuote;
    }
}
