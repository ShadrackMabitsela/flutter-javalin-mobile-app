package za.co.wethinkcode.qute.quotes.server.DataAccessLayer;

import org.json.JSONArray;

import java.util.List;
import java.util.Map;

public interface QuoteDB {
    /**
     * Get all quotes in the database
     * @return JSONArray
     */
    JSONArray all() throws Exception;

    /**
     * Add a single quote to the database.
     * @param authorName String
     * @param quteQuote String
     */
    void add(String authorName, String quteQuote);

    /**
     * Delete a single quote by id.
     * @param id the id of the quote
     */
    void delete(Integer id) throws ClassNotFoundException;
}
