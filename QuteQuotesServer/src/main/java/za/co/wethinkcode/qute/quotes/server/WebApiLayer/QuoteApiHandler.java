package za.co.wethinkcode.qute.quotes.server.WebApiLayer;

import io.javalin.http.Context;
import io.javalin.http.HttpCode;
import za.co.wethinkcode.qute.quotes.server.DataAccessLayer.QuoteDB;
import za.co.wethinkcode.qute.quotes.server.DataAccessLayer.QuoteDatabase;

public class QuoteApiHandler {
    private static final QuoteDB database = new QuoteDatabase();

    /**
     * Get all quotes
     * @param context The Javalin Context for the HTTP GET Request
     */
    public static void getAll(Context context) throws Exception {
        String temp = database.all().toString();
        context.json(temp);
    }

    /**
     * Create a new quote
     * @param context The Javalin Context for the HTTP POST Request
     */
    public static void add(Context context) {
        String authorName = context.pathParam("author");
        String quteQuote = context.pathParam("quote");
        database.add(authorName, quteQuote);
        context.status(HttpCode.CREATED);
        context.json("New Qute Quote created successfully");
    }

    /**
     * Delete a qute quote
     * @param context The Javalin Context for the HTTP GET Request
     */
    public static void delete(Context context) throws ClassNotFoundException {
        Integer id = context.pathParamAsClass("id", Integer.class).get();
        database.delete(id);
        context.status(HttpCode.OK);
        context.json("Qute Quote deleted successfully");
    }
}
