package za.co.wethinkcode.qute.quotes.server.DataAccessLayer;

import org.json.JSONArray;

public class QuoteDatabase implements QuoteDB {
    private static final String DB_NAME = "QuteQuotes.db";

    @Override
    public JSONArray all() {
        DbConnect dbConnection = new DbConnect(DB_NAME);
        return dbConnection.getAllQuteQuotes();
    }

    @Override
    public void add(String authorName, String quteQuote) {
        DbConnect dbConnection = new DbConnect(DB_NAME);
        dbConnection.insertNewQuteQuote(authorName, quteQuote);
    }

    @Override
    public void delete(Integer id) throws ClassNotFoundException {
        DbConnect dbConnection = new DbConnect(DB_NAME);
        dbConnection.deleteQuteQuote(id);
    }
}
