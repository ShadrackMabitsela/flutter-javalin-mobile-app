package za.co.wethinkcode.qute.quotes.server.WebApiLayer;

import io.javalin.Javalin;

public class QuoteServer {
    private final Javalin server;

    public QuoteServer() {
        server = Javalin.create(config -> config.defaultContentType = "application/json");

        // Before turning the following to Reference calls:
        // 'QuoteApiHandler::getAll' was 'context -> QuoteApiHandler.getAll(context)'
        // 'QuoteApiHandler::getOne' was 'context -> QuoteApiHandler.getOne(context)'
        // 'QuoteApiHandler::create' was 'context -> QuoteApiHandler.create(context)'
        this.server.get("/quotes", QuoteApiHandler::getAll);
        this.server.post("/quote/{author}/{quote}", QuoteApiHandler::add);
        this.server.delete("/quote/{id}", QuoteApiHandler::delete);
    }

    public void start(int port) { this.server.start(port); }

    public void stop() { this.server.stop(); }

    public static void main(String[] args) {
        QuoteServer server = new QuoteServer();
        server.start(5050);
    }
}
