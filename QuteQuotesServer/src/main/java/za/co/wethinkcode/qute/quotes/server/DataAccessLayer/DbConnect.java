package za.co.wethinkcode.qute.quotes.server.DataAccessLayer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sqlite.SQLiteConfig;
import java.sql.*;

public class DbConnect {
    private final String DB_URL;

    public DbConnect(String dbName) {
        DB_URL = "jdbc:sqlite:" + dbName;
        try(Connection connection = DriverManager.getConnection(DB_URL)) {
        }
        catch (SQLException e) { throw new RuntimeException(e); }
    }

    public void insertNewQuteQuote(String authorName, String quteQuote) {
        try(Connection connection = DriverManager.getConnection(DB_URL)) {
            try( final PreparedStatement stmt = connection.prepareStatement (
                    "INSERT INTO tbl_quotes(author_name, qute_quote) VALUES (?, ?)"
            )) {
                stmt.setString( 1, authorName );
                stmt.setString( 2, quteQuote );
                final boolean gotAResultSet = stmt.execute();

                if(gotAResultSet) {
                    throw new RuntimeException( "Unexpectedly got a SQL result-set." );
                } else {
                    final int updateCount = stmt.getUpdateCount();
                    if(updateCount == 1) {
                        System.out.println( "1 row INSERTED into tbl_quotes" );
                    } else {
                        throw new RuntimeException("Expected 1 row to be inserted, but got " + updateCount + ".");
                    }
                }
            }
        }
        catch (SQLException e) { throw new RuntimeException(e); }
    }

    public void deleteQuteQuote(Integer quoteId) throws ClassNotFoundException {
        String DRIVER = "org.sqlite.JDBC";
        Class.forName(DRIVER);
        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true);

        try(Connection connection = DriverManager.getConnection(DB_URL, config.toProperties())) {
            try( final PreparedStatement stmt = connection.prepareStatement (
                    "DELETE FROM tbl_quotes WHERE quote_id = ?;"
            )) {
                stmt.setInt(1, quoteId);
                final boolean gotAResultSet = stmt.execute();

                if (gotAResultSet)
                    throw new RuntimeException("Unexpectedly got a SQL result-set after deleting a qute quote.");
                else
                    System.out.println("The qute quote with id: '" + quoteId + "' was deleted from the database.");
            }
        }
        catch (SQLException e) { throw new RuntimeException(e); }
    }

    public JSONArray getAllQuteQuotes() {
        try(Connection connection = DriverManager.getConnection(DB_URL)) {
            try( final PreparedStatement stmt = connection.prepareStatement (
                    "SELECT * FROM tbl_quotes"
            )) {
                final boolean gotAResultSet = stmt.execute();

                // The key thing, which you need to remember is that initially, the ResultSet's cursor points
                // to before the first row when you call the next() method it points to the first row.
                ResultSet resultSet = stmt.getResultSet();
                JSONArray jsonArray = new JSONArray();

                while (resultSet.next()) {
                    int columns = resultSet.getMetaData().getColumnCount();
                    JSONObject obj = new JSONObject();
                    for (int i = 0; i < columns; i++)
                        obj.put(resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase(), resultSet.getObject(i + 1));
                    jsonArray.put(obj);
                }
                return jsonArray;
            }
        }
        catch (SQLException e) { throw new RuntimeException(e); }
    }
}
